package com.example.muse;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class ActivityDetail extends AppCompatActivity {

    String equipment_id;
    RvAdapter adapter;

    @Override
        protected void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_detail);

            Intent intent = getIntent();

        final Equipment equipment = intent.getParcelableExtra("OBJET");
        equipment_id = equipment.getIdName();

        GetDateAsync DateA = new GetDateAsync();
        DateA.execute();

        TextView name = findViewById(R.id.name);
        name.setText(equipment.getName());

        ImageView img = findViewById(R.id.img);
        try{
            String urlImg = WebServiceUrlMuseum.buildGetThumbnail(equipment_id).toString();
            Picasso.get().load(urlImg).fit().into(img);
        }
        catch (Exception e){
            e.printStackTrace();
        }
		if(equipment.getYear() != null){
			TextView annee = findViewById(R.id.annee2);
			annee.setText(equipment.getYear());
		}

        TextView categories = findViewById(R.id.categories2);
        categories.setText(TextUtils.join(" - ", equipment.getCategories()));

        TextView descr = findViewById(R.id.description2);
        descr.setText(equipment.getDescr());

        TextView timeFrame = findViewById(R.id.timeFrame2);
        timeFrame.setText(TextUtils.join(" - ", equipment.getTimeFrame()));

        if(equipment.getBrand() != null){
			TextView brand = findViewById(R.id.brand2);
			brand.setText(equipment.getBrand());
		}

        if(equipment.getTechDetails() != null){
			TextView techDetail = findViewById(R.id.techDetails2);
			techDetail.setText(TextUtils.join(" - ", equipment.getTechDetails()));
		}

        if(equipment.getWorking() != null){
			TextView work = findViewById(R.id.working2);
			if(equipment.getWorking() != null){
                if(equipment.getWorking().toLowerCase().contains("true")){
                    work.setText("OUI");
                }
                else{
                    work.setText("NON");
                }
            }

		}

       

		if(!((equipment.getPicture().get(0)).equals("null"))){
			Button button = findViewById(R.id.button);
			button.setOnClickListener(new View.OnClickListener(){
				public void onClick(View v){
					Intent intent = new Intent(getApplicationContext(), ActivityImages.class);
					intent.putExtra("IMG", equipment);
					v.getContext().startActivity(intent);
				}
			});
		}
		else{
            Button button = findViewById(R.id.button);
            button.setText("Aucune image disponible");
            button.setTextColor(Color.parseColor("#ff0011"));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                button.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.backg)));
            }

        }

    }

    public class GetDateAsync extends AsyncTask<String, String, String> {

        ArrayList<String> DATE = new ArrayList<String>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            URL url;
            HttpURLConnection urlConnection = null;
            try{
                url = WebServiceUrlMuseum.buildGetDemo();
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream input = urlConnection.getInputStream();
                JSONResponseHandlerDate JSONdate = new JSONResponseHandlerDate(DATE);
                JSONdate.readJsonStream(input);
            }
            catch (Exception e){
                e.printStackTrace();
            }
            finally {
                if(urlConnection != null){
                    urlConnection.disconnect();
                }
            }

            return "true";
        }

        @Override
        protected void onPostExecute(String s) {
            for(String elt : DATE){
                String[] elt2 = elt.split(":");
                if((elt2[0]).equals(equipment_id)){
                    TextView date = findViewById(R.id.next2);
                    date.setText(elt2[1]);
                }
            }
        }
    }
}
