package com.example.muse;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ActivityImages extends AppCompatActivity {

    RvAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_rv);

        Intent intent = getIntent();

        final Equipment obj = intent.getParcelableExtra("IMG");

        RecyclerView rv = (RecyclerView) findViewById(R.id.recviewlist);
        rv.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RvAdapter(obj.getPicture(), obj.getIdName());
        rv.setAdapter(adapter);
    }
}
