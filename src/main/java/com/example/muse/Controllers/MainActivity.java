package com.example.muse;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.ListView;
import android.widget.SearchView;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView myListView;
    Cursor result;
    EquipmentDbHelper dbHelper;
    MyAdapter adapter;
    ArrayList<Object> listObj = new ArrayList<Object>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        dbHelper = new EquipmentDbHelper(this);
        if(dbHelper.fetchAllObjet().getCount() < 1)
        {

            FillMuseumAsync fillM = new FillMuseumAsync();
            fillM.execute();

        }

        listObj = dbHelper.getAllObjet("name asc");
        RecyclerView rv = (RecyclerView) findViewById(R.id.recview);
        rv.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MyAdapter(listObj);
        rv.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.app_bar_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.alpha) {
            listObj.clear();
            listObj.addAll(dbHelper.getAllObjet("name asc"));
            adapter.notifyDataSetChanged();
            return true;
        }
        if (id == R.id.chrono) {
            listObj.clear();
            listObj.addAll(dbHelper.getAllObjet("year asc"));
            adapter.notifyDataSetChanged();
            return true;
        }
        if (id == R.id.categ) {

            GetAllCategAsync allCatAsync = new GetAllCategAsync();
            allCatAsync.execute();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    public class FillMuseumAsync extends AsyncTask<String, String, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {

            ArrayList<String> ids = new ArrayList<String>();
            Equipment equipment;
            URL url;
            URL url2;
            HttpURLConnection urlConnection = null;
            try{
                url = WebServiceUrlMuseum.buildGetAllId();
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream input = urlConnection.getInputStream();
                JSONResponseHandlerIds JSONids = new JSONResponseHandlerIds(ids);
                JSONids.readJsonStream(input);
            }
            catch (Exception e){
                e.printStackTrace();
            }
            finally {
                if(urlConnection != null){
                    urlConnection.disconnect();
                }
            }

            for(String value : ids){
                try{
                    url2 = WebServiceUrlMuseum.buildGetItemInfo(value);
                    urlConnection = (HttpURLConnection) url2.openConnection();
                    InputStream input2 = urlConnection.getInputStream();
                    equipment = new Equipment(value);
                    JSONResponseHandlerEquipment JSONobj = new JSONResponseHandlerEquipment(equipment);
                    JSONobj.readJsonStream(input2);
                    dbHelper.addObjet(equipment);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                finally {
                    if(urlConnection != null){
                        urlConnection.disconnect();
                    }
                }
            }

            return "true";
        }

        @Override
        protected void onPostExecute(String s) {
            listObj.clear();
            listObj.addAll(dbHelper.getAllObjet("name asc"));
            adapter.notifyDataSetChanged();
        }
    }

    public class GetAllCategAsync extends AsyncTask<String, String, String>{

        ArrayList<String> cat = new ArrayList<String>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            URL url3;
            HttpURLConnection urlConnection = null;
            try{
                url3 = WebServiceUrlMuseum.buildGetAllCat();
                urlConnection = (HttpURLConnection) url3.openConnection();
                InputStream input = urlConnection.getInputStream();
                JSONResponseHandlerCategorie JSONcat = new JSONResponseHandlerCategorie(cat);
                JSONcat.readJsonStream(input);
            }
            catch (Exception e){
                e.printStackTrace();
            }
            finally {
                if(urlConnection != null){
                    urlConnection.disconnect();
                }
            }

            return "true";
        }

        @Override
        protected void onPostExecute(String s) {
            listObj.clear();
            listObj.addAll(dbHelper.getObjetByCat(cat));
            adapter.notifyDataSetChanged();
        }
    }
}
