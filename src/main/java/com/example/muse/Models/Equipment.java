package com.example.muse;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Equipment implements Parcelable {

    public static final String TAG = Equipment.class.getSimpleName();

    private long id; // used for the _id colum of the db helper

    private String id_name;
    private String name;
    private String year;
    private ArrayList<String> categories = new ArrayList<String>();
    private String descr;
    private ArrayList<String> timeFrame = new ArrayList<String>();
    private String brand;
    private ArrayList<String> techDetails = new ArrayList<String>();
    private String working;
    private ArrayList<String> picture = new ArrayList<String>();

    public Equipment(String id_name) {
        this.id_name = id_name;
        this.picture.add(null);
    }

    public Equipment(long id, String id_name, String name, String year, ArrayList<String> categories, String descr, ArrayList<String> timeFrame, String brand, ArrayList<String> techDetails, String working, ArrayList<String> picture) {
        this.id = id;
        this.id_name = id_name;
        this.name = name;
        this.year = year;
        this.categories = categories;
        this.descr = descr;
        this.timeFrame = timeFrame;
        this.brand = brand;
        this.techDetails = techDetails;
        this.working = working;
        this.picture = picture;
    }

    public long getId() {
        return id;
    }

    public String getIdName() {
        return id_name;
    }

    public String getName() {
        return name;
    }

    public String getYear() {
        return year;
    }

    public ArrayList<String> getCategories() {
        return categories;
    }

    public String getDescr() {
        return descr;
    }

    public ArrayList<String> getTimeFrame() {
        return timeFrame;
    }

    public String getBrand() {
        return brand;
    }

    public ArrayList<String> getTechDetails() {
        return techDetails;
    }

    public String getWorking() {
        return working;
    }

    public ArrayList<String> getPicture() {
        return picture;
    }


    public void setId(long id) {
        this.id = id;
    }

    public void setIdName(String id_name) {
        this.id_name = id_name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setCategories(ArrayList<String> categories) {
        this.categories = categories;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public void setTimeFrame(ArrayList<String> timeFrame) {
        this.timeFrame = timeFrame;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setTechDetails(ArrayList<String> techDetails) {
        this.techDetails = techDetails;
    }

    public void setWorking(String working) {
        this.working = working;
    }

    public void clearPict(){
        this.picture.clear();
    }

    public void setPicture(ArrayList<String> picture) {
        this.picture = picture;
    }


    @Override
    public String toString() {
        return this.name+" = "+this.id_name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(id_name);
        dest.writeString(name);
        dest.writeString(year);
        dest.writeList(categories);
        dest.writeString(descr);
        dest.writeList(timeFrame);
        dest.writeString(brand);
        dest.writeList(techDetails);
        dest.writeString(working);
        dest.writeList(picture);
    }


    public static final Creator<Equipment> CREATOR = new Creator<Equipment>()
    {
        @Override
        public Equipment createFromParcel(Parcel source)
        {
            return new Equipment(source);
        }

        @Override
        public Equipment[] newArray(int size)
        {
            return new Equipment[size];
        }
    };

    public Equipment(Parcel in) {
        this.id = in.readLong();
        this.id_name = in.readString();
        this.name = in.readString();
        this.year = in.readString();
        this.categories = in.readArrayList(null);
        this.descr = in.readString();
        this.timeFrame = in.readArrayList(null);
        this.brand = in.readString();
        this.techDetails = in.readArrayList(null);
        this.working = in.readString();
        this.picture = in.readArrayList(null);
    }
}
