package com.example.muse;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

//Parse info from /categories

public class JSONResponseHandlerCategorie {

    private static final String TAG = JSONResponseHandlerCategorie.class.getSimpleName();

    private ArrayList<String> CAT = new ArrayList<String>();


    public JSONResponseHandlerCategorie(ArrayList<String> CAT) {
        this.CAT = CAT;
    }

    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readCat(reader);
        } finally {
            reader.close();
        }
    }

    public void readCat(JsonReader reader) throws IOException {
        reader.beginArray();
        while (reader.hasNext()) {
            CAT.add(reader.nextString());
        }
        reader.endArray();
    }
}
