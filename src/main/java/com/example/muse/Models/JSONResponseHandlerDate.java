package com.example.muse;

//Parse info from /demos

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class JSONResponseHandlerDate {

    private static final String TAG = JSONResponseHandlerDate.class.getSimpleName();

    private ArrayList<String> DATE = new ArrayList<String>();


    public JSONResponseHandlerDate(ArrayList<String> DATE) {
        this.DATE = DATE;
    }

    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readDate(reader);
        } finally {
            reader.close();
        }
    }

    public void readDate(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            readDateObjet(reader);
        }
        reader.endObject();
    }

    private void readDateObjet(JsonReader reader) throws IOException {
        while (reader.hasNext()) {
            String obj = reader.nextName();
            DATE.add(obj.concat(":".concat(reader.nextString())));

        }
    }
}
