package com.example.muse;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

//Parse info from /ids

public class JSONResponseHandlerIds {

    private static final String TAG = JSONResponseHandlerIds.class.getSimpleName();

    private ArrayList<String> IDS = new ArrayList<String>();


    public JSONResponseHandlerIds(ArrayList<String> IDS) {
        this.IDS = IDS;
    }

    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readIds(reader);
        } finally {
            reader.close();
        }
    }

    public void readIds(JsonReader reader) throws IOException {
        reader.beginArray();
        while (reader.hasNext()) {
            IDS.add(reader.nextString());
        }
        reader.endArray();
    }
}
